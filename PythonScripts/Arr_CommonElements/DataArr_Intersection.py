#Data array arr_a and arr_b

# 2.1 Find the number of occurrences of each elements from array arr_a in arr_b
# 2.2 Generate a list arr_c consist of the previous elements appeared more than once (two ways)

#### 2.1
arr_a = ['a', 'b', 'c', 'd', 'e']
arr_b = ['j', 'a', 'x', 'f', 'e', 'd', 'c', 'w', 'a', 'd', 'e', 'e']

# Find Intersection between arr_a and arr_b

def common_elements1(list1, list2):
    return list(set(list1) & set(list2)) #(set(arr_a).intersection(arr_b))

def common_elements2(list1, list2):
    xs = [i for i in list1 if i in list2]
    return xs

print("".ljust(90,'~'))
print(" Find intersection between array 'Set intersection way' :")
print(common_elements1(arr_a, arr_b))
print("".ljust(90,'~'))

print(" Find intersection between array 'For In' way:")
print(common_elements2(arr_a, arr_b))
print("".ljust(90,'~'))

#Common elements 2.1 Find the number of occurrences of each elements from array arr_a in arr_b
sort_value = 1
def count_common_elements1(list_1, list_2, bound_value):
    cce_dict = dict()
    common_el = list(set(list_1) & set(list_2))
    for el in common_el:
        if list_2.count(el) >= bound_value:
            cce_dict.update({el: list_2.count(el)})
    return cce_dict

print("2.1 Find the number of occurrences of each elements from array arr_a in arr_b")
print(count_common_elements1(arr_a, arr_b, sort_value))
print("".ljust(90,'~'))

#### 2.2(1) Count common elements elements appeared more than once.
# Solution 1
sort_value2 = 2
def count_common_elements_2(list_1, list_2, bound_value):
    cce_dict = dict()
    common_el = list(set(list_1) & set(list_2))

    for el in common_el:
        if list_2.count(el) >= bound_value:
            cce_dict.update({el: list_2.count(el)})
    return cce_dict

print("2.2(1) Count common elements elements appeared more than once. Solution 1:")
print(count_common_elements_2(arr_a, arr_b, sort_value2))
print("".ljust(90,'~'))

#### 2.2(2) Count common elements elements appeared more than once.
#### Solution 2

from collections import Counter

arr_a = ['a', 'b', 'c', 'd', 'e']
arr_b = ['j', 'a', 'x', 'f', 'e', 'd', 'c', 'w', 'a', 'd', 'e', 'e']
sort_value2 = 2

def common_elements_2_2(list1, list2,bound_value):
    common = dict(Counter(list2))
    result = dict()
    for key, value in common.items():
        if (key in list1) and value >= bound_value:
            result.update({key:value})
    return result

print("2.2(2) Count common elements elements appeared more than once. Solution 2:")
print(common_elements_2_2(arr_a, arr_b, sort_value2))
print("".ljust(90,'~'))

#### 2.2(3) Count common elements elements appeared more than once.
#### Solution 3
print("2.2(3) Count common elements elements appeared more than once. Solution 3:")
import numpy as np

arr_a = ['a', 'b', 'c', 'd', 'e']
arr_b = ['j', 'a', 'x', 'f', 'e', 'd', 'c', 'w', 'a', 'd', 'e', 'e']

arrB = np.array(arr_b)
not_in_arrA_list = np.setdiff1d(arr_b, arr_a)
common_values_A_and_B = np.setdiff1d(arr_b, not_in_arrA_list)

uniqueValues, occurCount = np.unique(arrB, return_counts=True)

common_values = list(common_values_A_and_B)

keys = list(uniqueValues)
values = list(occurCount)
dictionary = dict(zip(keys, values))
print("".ljust(90,'~'))
print("- RESULTS -".center(90,'~'))

for (k, v) in dictionary.items():
    if k in common_values and v >= 2:
        print(f"{k} : {v}".center(90,' '))

print("".ljust(90,'~'))


#################################################
print("".ljust(90,'~'))
print(f"Original Array 'arr_b': {arrB}")
print(f"Common Values: {common_values_A_and_B}")
print("".ljust(90,'~'))
print(f"Unique Values    'arr_b': {uniqueValues}")
print(f"Occurrence Count 'arr_b': {occurCount}")
print("".ljust(90,'~'))
print(f"Zip to Dictionary: {dictionary}")
print("".ljust(90,'~'))
##################################################
