CONTENTS OF THE PROJECT
---------------------

 * e2e TestPlan
 * Dump of database and sql queries are in 'SQL_db_and_queries'
 * Python Scripts ' Find common elements in array'
 * Three levels of implementation/algoritms the Fibonacci sequence 
 * Top level overview of domain "House Prices in USA"/ Gistograms.
 * Review of 'log snipet'
 

e2e TestPlan
------------
- 'End to end' testing is to simulate the real user scenario and 
it also validates batch/data processing from other upstream/downstream systems.


1) Requirements.

- If a Requirements exist, process testing according to it + exploratory testing
- If Requirements  not relevant, start exploratory testing and create request 
to PM about creating spec

2) TestPlan - Precondition create 'artifact' - new row in DB
- Smoke check
- Posistive test sutes
- Negative test sutes

3) e2e USER SCENARIO

- Login -> Logout session states
- Editing and saving data
- Input and  output data for each system module


SQL QUERIES  
-------------
- queries are in /SQL_db_and_queries
- tables and db are in /SQL_db_and_queries/test_db_dump


Python Scripts Array
--------------------

- Folder: /PythonScripts/Arr_CommonElements (*.ipynb, *.py, *.pdf)


Three levels of algoritms the 'Fibonacci sequence' 
-------------------------------------------------

- Folder: /PythonScripts/Fibonacci_in_3_LevelImplemen (*.ipynb, *.py, *.pdf)



Top level Gistograms "House Prices in USA"
-------------------

- Folder: /Location_Price_DataAnalysis (*.ipynb, *.py, *.pdf)


Review of 'log snipet' Possible Issues
--------------------------------------
- Same 'timestamp' in one 'thread'
- Empty data for field 'logger'
- No 'transaction id' 
- No 'user id' 'usre status'
- if 'mlsId' the main parameter should be field for it
- 'updateCoomp' look like 'db coomit' message





MAINTAINERS
-----------

* Rizvash Iliya <rizvash.i@gmail.com>
