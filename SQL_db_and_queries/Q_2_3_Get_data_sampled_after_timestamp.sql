# QUERY 2.3 get data sampled after 2019-11-24:00:00:00

SET @from_data_query = "2019-11-24 00:00:00";

SELECT 
	upper(h.sample_name) AS PersonName,
	h.id AS h_ID,
	h.height_cm AS PersonHeight,
	fl.flavour AS Flavour,
    FROM_UNIXTIME(h.time) AS h_Time
FROM 
    heigh AS h LEFT JOIN flavours AS fl
ON 
    upper(h.sample_name) = upper(fl.names)
WHERE 
	(h.verified = "True" AND fl.verified = "True")
	 AND FROM_UNIXTIME(h.time) > @from_data_query
ORDER BY h_Time;

