CREATE VIEW commonTable AS
SELECT 
	upper(heigh.sample_name) AS hName,
	heigh.id AS h_ID,
	heigh.height_cm AS hHeight,
	flavours.flavour AS Flavour,
	photos.url AS url,
	
	JSON_OBJECT(
		'id', heigh.id,
		'name', heigh.sample_name,
		'height', heigh.height_cm,
		'flavour', flavours.flavour,
		'url_pic', photos.url) AS JsonObj
FROM 
    (heigh LEFT JOIN flavours
	ON upper(heigh.sample_name) = upper(flavours.names)) 
			LEFT JOIN photos
					ON upper(heigh.sample_name) = upper(photos.name)
                    
WHERE heigh.verified = "True" AND flavours.verified = "True"
ORDER BY h_ID;


# SELECT * FROM commonTable;
# SELECT hName, JsonObj FROM commonTable;
