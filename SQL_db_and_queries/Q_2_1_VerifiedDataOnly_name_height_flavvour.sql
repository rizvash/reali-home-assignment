SELECT 
	upper(h.sample_name) AS name,
	#h.id AS h_ID,
	h.height_cm AS height,
	fl.flavour AS flavour

FROM 
    heigh AS h LEFT JOIN flavours AS fl
ON 
    upper(h.sample_name) = upper(fl.names)
WHERE 
	h.verified = "True" AND (fl.verified = "True")  
    #h.verified = "True" and (fl.verified = "True" OR fl.verified is null);
ORDER BY name
LIMIT 5;