# Common data to json object
#
# -- heigh AS h
# -- flavours AS fl
# -- photos AS ph

SELECT 
	upper(h.sample_name) AS hName,
	h.id AS h_ID,
    h.height_cm AS hHeight,
    fl.flavour AS Flavour,
	ph.url AS url,
    
	JSON_OBJECT(
		'id', h.id,
		'name', h.sample_name,
		'height', h.height_cm,
		'flavour', fl.flavour,
		'url_pic', ph.url) AS JsonObj
FROM 
    (heigh AS h LEFT JOIN flavours AS fl
	ON upper(h.sample_name) = upper(fl.names)) 
			LEFT JOIN  photos AS ph
					ON upper(h.sample_name) = upper(ph.name)
                    
WHERE h.verified = "True" AND fl.verified = "True" 
ORDER BY h_ID;

